<?php
// $Id$

/**
 * @file
 * Install, update and uninstall functions for the BanHammer module.
 */


function banhammer_main($form_state, $status) {

  if ($status == 2) {
    // If the user has disabled spambot detection, send them back to the blacklist:
    if (variable_get('banhammer_spambot_blocking_enabled', 1) == 0) {
      drupal_set_message('Spambot detection has been ' . l('disabled', 'admin/user/banhammer/settings'), 'error', FALSE);
      drupal_goto('admin/user/banhammer');
    }
    else {
      banhammer_update_db();
    }
  }

  $form = array();
  $options = array();

  $head = array(
    array('data' => t('Host'),            'field' => 'host_ip',        'sort' => 'desc'),
    array('data' => t('Country'),         'field' => 'host_country',   ),
    array('data' => t('Failed Attempts'), 'field' => 'failed_attempts',),
    array('data' => t('Last Attempt'),    'field' => 'last_attempt',   ),
  );

  $limit  = variable_get('banhammer_report_limit', '25');

  if (db_result(db_query("SELECT COUNT(*) FROM {banhammer} WHERE status='%d'", $status)) > 0) {

    // $status is an internally-generated variable; users do not have the ability
    // to modify it. Hence, using it directly in an SQL statement is safe.
    $result = ($limit != 'all')
      ? pager_query("SELECT * FROM {banhammer} WHERE status='$status'" . tablesort_sql($head), $limit)
      : db_query("SELECT * FROM {banhammer} WHERE status='$status'" . tablesort_sql($head));

    while ($row = db_fetch_array($result)) {
      $options[$row['host_ip']]                 = '';
      $form[$row['host_ip']]['action']          = array('#value' => $row['host_ip']);      
      $form[$row['host_ip']]['host_ip']         = array('#value' => $row['host_ip']);
      $form[$row['host_ip']]['host_country']    = array('#value' => $row['host_country']);
      $form[$row['host_ip']]['failed_attempts'] = array('#value' => (empty($row['failed_attempts']))
        ? '<i>NULL</i>'
        : $row['failed_attempts']
      );
      $nodate = (empty($row['failed_attempts']))
          ? 'Imported from Access rules'
          : 'No data available';
      $form[$row['host_ip']]['last_attempt']    = array('#value' => (!empty($row['last_attempt']))
        ? format_date($row['last_attempt'], 'medium')
        : $nodate
      );
      $edit = (empty($row['message']) || $row['message'] == variable_get('banhammer_custom_message', ''))
        ? 'Create'
        : 'Edit';
      $form[$row['host_ip']]['edit']            = array('#value' => l($edit, 'admin/user/banhammer/edit-ban-message/' . $row['host_ip']));
    }

    $form['banhammer'] = array(
      '#type'        => 'checkboxes',
      '#title'       => 'Hosts',
      '#description' => 'These hosts are in the active list',
      '#options'     => $options,
    );

    $form['banhammer_actions'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Action to perform'),
      '#collapsible' => FALSE,
      '#prefix'      => '<div class="container-inline">',
      '#suffix'      => '</div>',
    );

    $form['banhammer_actions']['banhammer_actions_select'] = array(
      '#type'        => 'select',
      '#title'       => '',
      '#options'     => banhammer_get_actions($status),
    );

    $form['banhammer_actions']['banhammer_actions_submit'] = array(
      '#type'  => 'submit',
      '#value' => 'Go',
    );

    $form['banhammer_status'] = array(
      '#type'  => 'value',
      '#value' => $status,
    );

  }

  return $form;

}

function banhammer_main_validate($form, $form_state) {
  // Validation placeholder
}


// This function moves entries between BanHammer lists
function banhammer_main_submit($form, $form_state) {
  foreach ($form_state['values']['banhammer'] as $key => $value) {
    if (!empty($value) && $value !== 0) {
      if ($value == $_SERVER['REMOTE_ADDR'] && $form_state['values']['banhammer_actions_select'] == '0') {
        drupal_set_message('Safety check: Could not block ' . $value . ' because this is you. Consider whitelisting this IP address.', 'error', FALSE);
      }
      else {

        // Update the IP in BanHammer's list
        $object = array(
          'status'  => $form_state['values']['banhammer_actions_select'],
          'host_ip' => $value
        );
        drupal_write_record('banhammer', $object, 'host_ip');

        if ($form_state['values']['banhammer_actions_select'] === '1' ||
            $form_state['values']['banhammer_actions_select'] === '0') {
          // If this IP address is already listed in the access table, update it:
          if (db_result(db_query("SELECT COUNT(*) FROM {access} WHERE type='host' AND mask='%s'", $value)) > 0) {
            db_query("UPDATE {access} SET status='%d' WHERE type='host' AND mask='%s'", $form_state['values']['banhammer_actions_select'], $value);
          }
          else {
            // Create an entry for the access table
            $user   = array(
              'mask'   => $value,
              'type'   => 'host',
              'status' => $form_state['values']['banhammer_actions_select'],
            );
            drupal_write_record('access', $user);
          }
        }
        else {
          // If the host will not be blacklisted or whitelisted, remove it
          // from the access table:
          db_query("DELETE FROM {access} WHERE type='host' AND mask='%s'", $value);
        }
      }
    }
  }
}

function theme_banhammer_main($form) {

  $rows   = array();
  $custom = variable_get('banhammer_custom', 0);
  $limit  = variable_get('banhammer_report_limit', '25');

  // Define headers:
  $head = array(
    theme('table_select_header_cell'),
    array('data' => t('Host'),            'field' => 'host_ip',        'sort' => 'desc'),
    array('data' => t('Country'),         'field' => 'host_country',   ),
    array('data' => t('Failed Attempts'), 'field' => 'failed_attempts',),
    array('data' => t('Last Attempt'),    'field' => 'last_attempt',   ),
  );

  if ($custom == 1 && $form['banhammer_status']['#value'] == 0) {
    array_push($head, array('data' => t('Custom Message'),));
  }

  if (isset($form['banhammer']['#options'])) {

    $output .= drupal_render($form['banhammer_filters']);
    $output .= drupal_render($form['banhammer_actions']);

    foreach (element_children($form['banhammer']) as $key) {

      $row = array(
        drupal_render($form['banhammer'][$key]),
        drupal_render($form[$key]['action']),
        drupal_render($form[$key]['host_country']),
        drupal_render($form[$key]['failed_attempts']),
        drupal_render($form[$key]['last_attempt']),
      );

      if ($custom == 1 && $form['banhammer_status']['#value'] == 0) {
        array_push($row, drupal_render($form[$key]['edit']));
      }

      array_push($rows, $row);

      // Prevent Drupal from rendering a bunch of IP addresses after the table:
      drupal_render($form[$key]['host_ip']);
      drupal_render($form[$key]['edit']);
    }

    // Kill the elements we don't want to render:
    drupal_render($form['banhammer']);
    drupal_render($form['banhammer_submit']);

  }

  else {
    $rows[] = array(
      array(
        'data' => '<div>No IP addresses on this list.</div>',
        'colspan' => ($custom == 1)
          ? 6
          : 5,
      )
    ); 
  }

  // Render the output
  $output .= theme('table', $head, $rows);

  // Render the pager HTML, if the user wants it
  $output .= ($limit == "all")
    ? ''
    : theme('pager', NULL, $limit, 0);

//  if ($form['pager']['#value']) {
//    $output .= drupal_render($form['pager']);
//  }

  $output .= drupal_render($form);

  return $output;

}

function banhammer_get_countries() {
  $countries  = array();
  $result     = db_query("SELECT host_country FROM {banhammer} WHERE status='2'");

  while ($row = db_fetch_array($result)) {
    $countries[strtolower($row['host_country'])] = $row['host_country'];
  }
  return array_unique($countries);
}

function banhammer_get_actions($status) {
  $actions = array();
  switch ($status) {
    case 0:
      $actions = array(
        '2'    => 'Remove from blacklist',
        '1'    => 'Add to whitelist',
        '3'    => 'Remove and ignore',
      );
      break;
    case 1:
      $actions = array(
        '2'    => 'Remove from whitelist',
        '0'    => 'Add to blacklist',
        '3'    => 'Remove and ignore',
      );
      break;
    case 2:
      $actions = array(
        '0'    => 'Add to blacklist',
        '1'    => 'Add to whitelist',
        '3'    => 'Ignore for now',
      );
      break;
  }
  return $actions;
}

function banhammer_update_db() {

  // Initialize variables:
  $ips           = array();
  $timespan      = (variable_get('banhammer_timespan',  '7')) * 24 * 60 * 60;
  $start         = time() - $timespan;
  $threshold     = variable_get('banhammer_threshold', '3');
  $fetch_country = variable_get('banhammer_location', 0);

  // Build an array of IPs that have failed CAPTCHAs over the given time period
  $result    = db_query("SELECT * FROM {watchdog} WHERE type='%s' AND timestamp > '%d' ORDER BY timestamp ASC", variable_get('banhammer_captcha_module', ''), $start);
  while($row = db_fetch_array($result)) {
    $failures = (isset($ips[$row['hostname']]['count']))
      ? $ips[$row['hostname']]['count'] + 1
      : 1;
    $ips[$row['hostname']] = array(
      'count'   => $failures,
      'last'    => $row['timestamp'],
      'country' => variable_get('banhammer_geolocation_unknown', ''),
    );
  }

  // Check the IP array and eliminate entries that don't meet the threshold:
  foreach (array_keys($ips) as $key) {
    if ($ips[$key]['count'] < $threshold) {
      unset($ips[$key]);
    }
  }

  // Cross reference the IP array with the BanHammer database:
  $result = db_query("SELECT * FROM {banhammer}");
  while ($row = db_fetch_array($result)) {

    // If the host is already in the BanHammer database, drop it from the 
    // array and perform some other actions
    if (isset($ips[$row['host_ip']]) && in_array($row['host_ip'], array_keys($ips))) {

      $failures = ($ips[$row['host_ip']]['count'] > $row['failed_attempts'])
        ? $ips[$row['host_ip']]['count']
        : $row['failed_attempts'];

      $object = array(
        'host_ip'         => $row['host_ip'],
        'failed_attempts' => $failures,
        'last_attempt'    => $ips[$row['host_ip']]['last'],
      );

      // If the host has been previously ignored, but has since failed another CAPTCHA
      // then update its status:
      if ($row['status'] == '3' && $row['last_attempt'] < $ips[$row['host_ip']]['last']) {
        $object['status'] = 2;
      }

      // Drop host from the IP array:
      unset($ips[$row['host_ip']]);

      // Update the BanHammer database
      drupal_write_record('banhammer', $object, 'host_ip');

    }
  }

  // Anything left in the IP array is new. Add it to the database:
  if (!empty($ips)) {
    foreach (array_keys($ips) as $key) {
      $object = array(
        'host_ip'         => $key,
        'host_country'    => variable_get('banhammer_geolocation_unknown', ''),
        'failed_attempts' => $ips[$key]['count'],
        'last_attempt'    => $ips[$key]['last'],
        'status'          => 2,
      );
      drupal_write_record('banhammer', $object);
    }
  }
  // Finally, add in any countries that are not yet populated:
  $result = db_query("SELECT * FROM {banhammer} WHERE 
                 host_country=''
              OR host_country IS NULL
              OR host_country='%s'
              OR host_country='%s'
              OR host_country='%s'",
              variable_get('banhammer_geolocation_not_activated', ''),
              variable_get('banhammer_geolocation_failure',       ''),
              variable_get('banhammer_geolocation_unknown',       '')
            );
  while ($row = db_fetch_array($result)) {
    $object = array(
      'host_ip'         => $row['host_ip'],
      'host_country'    => banhammer_get_country($row['host_ip'], $fetch_country),
    );
    drupal_write_record('banhammer', $object, 'host_ip');
  }
}

function banhammer_get_country($ip, $fetch_country) {
  if ($fetch_country == 1) {
    switch (variable_get('banhammer_location_service', '')) {
      case "geoplugin":
        $data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
        if (empty($data) || empty($data['geoplugin_countryName'])) {
          return variable_get('banhammer_geolocation_failure', '');
        }
        return $data['geoplugin_countryName'];
        break;
    }
  }
  else {
    return variable_get('banhammer_geolocation_not_activated', '');
  }
}

function banhammer_custom_messaging($form_state, $ip) {

  $check = db_fetch_array(db_query("SELECT * FROM {banhammer} WHERE host_ip='%s' AND status='0'", $ip));

  if (!empty($check)) {
    $form = array();

    $form['banhammer_ip'] = array(
      '#type'  => 'value',
      '#value' => $ip,
    );

    $form['banhammer_ip_message'] = array(
      '#type'          => 'textarea',
      '#title'         => 'Message',
      '#description'   => 'This is the message that will be displayed when ' . $ip . ' attempts to acess the site.',
      '#default_value' => (!empty($check['message']))
        ? $check['message']
        : variable_get('banhammer_custom_message', ''),
    );

    $form['banhammer_ip_message_submit'] = array(
      '#type'  => 'submit',
      '#value' => 'Save',
    );

    $form['banhammer_ip_message_cancel'] = array(
      '#type'  => 'submit',
      '#value' => 'Cancel',
    );

    $form['#redirect'] = 'admin/user/banhammer/blacklist';

    return $form;
  }
  else {
    drupal_set_message('The IP address you entered, "' . $ip . '" is not in the blacklist database.', 'error', FALSE);
    drupal_goto('admin/user/banhammer/blacklist');
  }
}

function banhammer_custom_messaging_submit($form, $form_state) {

  $form = array();

  if ($form_state['values']['op'] == "Cancel") {
    drupal_set_message('Action canceled', 'status', FALSE);
    return $form;
  }

  $object = array(
    'host_ip' => $form_state['values']['banhammer_ip'],
    'message' => $form_state['values']['banhammer_ip_message'],
  );

  if (drupal_write_record('banhammer', $object, 'host_ip')) {
    drupal_set_message('Record updated', 'status', FALSE);
  }
  else {
    drupal_set_message('Failed to save changes', 'error', FALSE);
  }

  return $form;

}
