<?php
/**
 * Define the settings form.
 */
function banhammer_admin_settings() {

  $form['banhammer_cron'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Blacklist spammers on CRON run?'),
    '#default_value' => variable_get('banhammer_cron', 1),
    '#description'   => t('If checked, BanHammer will blacklist all new spam hosts every time cron runs.'),
  );

  $form['banhammer_timespan'] = array(
    '#title'         => 'How many days back should BanHammer look for potential spammers?',
    '#type'          => 'textfield',
    '#description'   => 'BanHammer will check for hosts that have repeatedly failed CAPTCHAs over the past <i>x</i> days. <b>NOTE: Drupal is set to remove log entries <u>older than ' . format_interval(variable_get('statistics_flush_accesslog_timer', 259200)) . '</u>, or <u>' . variable_get('dblog_row_limit', 1000) . ' entries</u>, whichever comes first.</b> How far back you can look depends on these variables; If you configure this to a point outside the range of available data, BanHammer will look as far back as possible and stop at the end of the database.',
    '#default_value' => variable_get('banhammer_timespan', '7'),
    '#required'      => TRUE,
  );
  
  $form['banhammer_threshold'] = array(
    '#title'         => 'Spammer Threshold',
    '#type'          => 'textfield',
    '#description'   => 'This is the number of failed CAPTCHA attempts a host must have to be considered a spammer.',
    '#default_value' => variable_get('banhammer_threshold', '3'),
    '#required'      => TRUE,
  );
  
  $form['banhammer_report_limit'] = array(
    '#type'          => 'select',
    '#title'         => t('Report Table Length'),
    '#default_value' => variable_get('banhammer_report_limit', '25'),
    '#options'       => array(
                          '10'  => '10',
                          '25'  => '25',
                          '50'  => '50',
                          '100' => '100',
                          'all' => 'All',
    ),
    '#description'   => t('When BanHammer displays lists of hosts (possible spammers, blacklisted hosts, whitelisted hosts, etc), it provides the information in a sortable table. This table may be very long, so BanHammer can break it up into pages for easier reading. This option allows you to set how many items are displayed at one time.'),
  );
  
  // Create a fieldset to contain the beta features of this function
  $form['banhammer_beta_features'] = array(
    '#title'       => 'Beta Features',
    '#type'        => 'fieldset',
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  
  $form['banhammer_beta_features']['banhammer_location'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('<b>Include host location in reports?</b>'),
    '#default_value' => variable_get('banhammer_location', 0),
    '#description'   => t('BanHammer will use the free geolocation database at <a href="http://www.geoplugin.net">geoplugin.net</a> to determine the country where the host is located and include this information in the reports. In order for this to work properly, you must register your domain so that the service will report back.'),
  );
  
  $form['banhammer_beta_features']['banhammer_clear_table_on_threshold_update'] = array(
    '#title'         => 'Update tables with threshold?',
    '#type'          => 'checkbox',
    '#description'   => 'The Autoblocker table contains a list of all hosts, geolocations, access status, failed CAPTCHAs, etc. If this box is checked, then whenever the threshold value changes, BanHammer will automatically drop hosts that no longer meet the threshold. For example: If your threshold is set to 2 failed CAPTCHAs in a 7 day period, then any host meeting this requirement will be banned. If you update the threshold to 4 failed CAPTCHAs, then any host with only 2 failures will be automatically removed from the blacklist until such time as they reach the 4-failure limit.',
    '#default_value' => variable_get('banhammer_clear_table_on_threshold_update', '0'),
  );

  return system_settings_form($form);

}

/**
 * Display a list of all IP addresses for each list
 *
 * When a user wants to inspect the entries on a given list, this function will
 * generate that list in a sortable, paged table. This table includes information
 * on how many CAPTCHAs the IP failed, when it last failed, and - if the user has
 * configured it - geolocation data for the IP.
 *
 * @param $status
 *   Simple string of which list to show ("blacklist","whitelist","potential")
 *
 * @return
 *   HTML for a sortable, paged table containing info on the members of a given list
 */
function banhammer_host_list($status) {

 /**
  * If user wants to see all potential spammers, go to the function
  * specifically written for that.
  * TO DO: Merge these two functions
  */
  if ($status == "potential") {
    return banhammer_potential_spammers_display();
  }

  // If no hosts are on the requested list, then return a simple message
  if (db_result(db_query("SELECT COUNT(*) FROM {banhammer} WHERE status='%s'", $status)) == 0) {
    return "No hosts have been " . $status . "ed. ";
  }

 /**
  * Generate a table head based on whether or not the user has configured the 
  * module to include location data
  */
  $head = (variable_get('banhammer_location', 0) == 0) ? array(
    array('data' => t('Host'),                   'field' => 'host_ip'          ),
    array('data' => t('No. of Failed Attempts'), 'field' => 'failed_attempts', 'sort' => 'desc'),
    array('data' => t('Last Attempt'),           'field' => 'last_attempt'     ),
    array('data' => t('Actions')),
  ):
  array(
    array('data' => t('Host'),                   'field' => 'host_ip'         ),
    array('data' => t('Location'),               'field' => 'host_country',   'sort' => 'asc'),
    array('data' => t('No. of Failed Attempts'), 'field' => 'failed_attempts' ),
    array('data' => t('Last Attempt'),           'field' => 'last_attempt'    ),
    array('data' => t('Actions')),
  );

  // Begin the SQL statement
  $sql = str_replace("%s", db_escape_string($status), "SELECT * FROM {banhammer} WHERE status='%s'" . tablesort_sql($head));
  
 /** 
  * If the user does not want to break up table results, then return everything
  * Otherwise, run a pager_query
  */
  if (variable_get('banhammer_report_limit', '25') == "all") {
    $result = db_query($sql);
  }
  else {
    $limit  = variable_get('banhammer_report_limit', '25');
    $result = pager_query($sql, $limit);
  }
  
  // Build the data array
  while ($row = db_fetch_array($result)) {
    $actions  = ($status == "blacklist") ? '<a href="../../../admin/user/banhammer/blacklist/remove/' . $row['host_ip'] . '">' . t('Remove') . '</a> | <a href="../../../admin/user/banhammer/whitelist/add/' . $row['host_ip'] . '">' . t('Add to Whitelist') . '</a>':'<a href="../../../admin/user/banhammer/whitelist/remove/' . $row['host_ip'] . '">' . t('Remove') . '</a> | <a href="../../../admin/user/banhammer/blacklist/add/' . $row['host_ip'] . '">' . t('Add to Blacklist') . '</a>';
    $data[]   = (variable_get('banhammer_location', 0) == 0) ? array(
      'data'  => array($row['host_ip'], $row['failed_attempts'], date(variable_get('date_format_long', 'l, F j, Y - H:i'), $row['last_attempt']), $actions),
      'class' => "",
    ):
    array(
      'data'  => array($row['host_ip'], $row['host_country'], $row['failed_attempts'], date(variable_get('date_format_long', 'l, F j, Y - H:i'), $row['last_attempt']), $actions),
      'class' => "",
    );
  }
  
  // Render the table HTML
  $table_output = theme('table', $head, $data);
  
  // Render the pager HTML, if the user wants it
  $pager_output = (variable_get('banhammer_report_limit', '25') == "all") ? "" : theme('pager', NULL, $limit, 0);

  // Return the combined table and pager HTML
  return $table_output . $pager_output;
}

/**
 * Generate a confirmation form for bulk-adding IP addresses to the blacklist
 *
 * When a user requests to bulk-add IP addresses to the blacklist, this function
 * intercepts the request and creates a confirmation form. The user is asked if
 * they truly want to ban all of these IP addresses.
 *
 * @param &$form_state
 *   This is a variable "reference" to the $form_state array created by Drupal.
 *
 * @return
 *   HTML for the confirmation form
 */
function banhammer_blacklist_spammers_confirm(&$form_state) {

  // Get the destination:
  $destination = check_plain($_GET['destination']);

  // Count how many hosts will be blacklisted
  $host_count = check_plain(banhammer_count_spam_hosts());

  // Initialize output and form variables
  $output = NULL;
  $form['_spammers'] = array(
    '#type'  => 'value',
    '#value' => 'true'
  );
  
 /**
  * Generate a confirmation form. If the user confirms, the function 'banhammer_blacklist_spammers_confirm_submit'
  * will be called by default. If the user cancels, they will be redirected to the module's landing page
  */
  $output = confirm_form(
    $form,
    t('Are you sure you want to add these hosts to the blacklist?'),
    isset($destination) ? $destination : "admin/user/banhammer",
    '<p>' . t("You are about to add <b>" . $host_count . "</b> hosts to the blacklist. You may remove these hosts from the blacklist manually.") . '</p>',
    t('Add to blacklist'),
    t('Cancel')
  );
  return $output;

}


/**
 * Generate a confirmation page for modifying the tables
 *
 * When the user attempts to modify a table entry (such as removing an IP from the
 * blacklist, or moving an IP from the whitelist to the blacklist), this function
 * interjects a form confirming the request.
 *
 * @param &$form_state
 *   Variable "reference" to the $form_state array created by Drupal.
 * @param $list
 *   Simple string specifying which list to alter
 * @param $action
 *   Simple string specifying the action to be performed
 * @param $ip
 *   Simple string specifying which IP address will be altered
 *
 * @return
 *   HTML for the confirmation form
 */
function banhammer_host_modify_confirm(&$form_state, $list, $action, $ip) {

  // Get the destination:
  $destination = check_plain($_GET['destination']);

  // Sanitize the inputs:
  $list_clean   = check_plain($list);
  $action_clean = check_plain($action);
  $ip_clean     = check_plain($ip);

  // Simple variable to get the correct grammar
  $v_clean = ($action_clean == "add") ? "to" : "from";

 /**
  * Initialize the output and form variables.
  * Include the list, action and ip variables in the form
  * variable, so they are passed along if the user confirms
  */
  $output = NULL;
  $form['_hosts'] = array(
    '#type'               => 'value',
    '#value'              => 'true',
    '#banhammer_list'   => $list_clean,
    '#banhammer_action' => $action_clean,
    '#banhammer_host'   => $ip_clean,
  );
  
 /**
  * Render the confirmation form.
  * If the user confirms, then the form variable will be passed to 'banhammer_host_modify_confirm_submit'
  * by default. If the user cancels, then they will be redirected to 'admin/user/banhammer/[original list]'
  */
  $output = confirm_form(
    $form,
    t('Are you sure you want to ' . $action_clean . ' host ' . $ip_clean . ' ' . $v_clean . ' the ' . $list_clean . '?'),
    isset($destination) ? $destination : "admin/user/banhammer/" . $list_clean,
    '<p>' . t("You are about to " . $action_clean . " host " . $ip_clean . " " . $v_clean . " the " . $list_clean . ".") . '</p>',
    t(drupal_ucfirst($action_clean) . ' ' . $v_clean . ' ' . $list_clean),
    t('Cancel')
  );
  
  // Return the form
  return $output;

}
