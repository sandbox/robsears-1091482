<?php
// $Id$

/**
 * @file
 * Install, update and uninstall functions for the BanHammer module.
 */

/**
 * Implementation of hook_install().
 */
function banhammer_install() {
  if (!drupal_install_schema('banhammer')) {
    drupal_set_message(st('Failed to install the database schema'), 'error', FALSE);
  }
}

/**
 * Implementation of hook_install().
 */
function banhammer_enable() {
  variable_set('banhammer_clear_table_on_threshold_update', '');
  variable_set('banhammer_cron',                            '');
  variable_set('banhammer_custom',                          '');
  variable_set('banhammer_custom_message',                  'You have been banned!');
  variable_set('banhammer_geolocation_failure',             'Failed to identify');
  variable_set('banhammer_geolocation_not_activated',       'Not checked');
  variable_set('banhammer_geolocation_unknown',             'Unknown Origin');
  variable_set('banhammer_import',                          '');
  variable_set('banhammer_location',                        '');
  variable_set('banhammer_location_service',                '');
  variable_set('banhammer_report_limit',                    '');
  variable_set('banhammer_spambot_blocking_enabled',        '');
  variable_set('banhammer_threshold',                       '');
  variable_set('banhammer_timespan',                        '');
}


/**
 * Implementation of hook_schema().
 */
function banhammer_schema() {
  $schema['banhammer'] = array(
    'description' => 'Contains host IP, geolocation, failed CAPTCHA attempts and timestamps',
    'fields' => array(
      'host_ip' => array(
        'description' => 'Host IP Address',
        'type' => 'varchar', 
        'length' => 255, 
      ),
      'host_country' => array(
        'description' => 'Host Geolocation',
        'type' => 'text',
        'size' => 'normal',
      ),
      'failed_attempts' => array(
        'description' => 'Number of failed CAPTCHA attempts',
        'type' => 'int',
        'size' => 'normal',
      ),
      'last_attempt' => array(
        'description' => 'Last Attempt',
        'type' => 'int',
        'size' => 'normal',
      ),
      'status' => array(
        'description' => 'Status',
        'type' => 'int',
        'size' => 'small',
        'default' => '2',
      ),
      'message' => array(
        'description' => 'Custom Message',
        'type' => 'text',
        'size' => 'medium',
        'default' => '',
      ),
    ),
    'primary key' => array('host_ip'),
  );
  return $schema;
}

/**
 * Implementation of hook_uninstall().
 */
function banhammer_uninstall() {
  variable_del('banhammer_clear_table_on_threshold_update');
  variable_del('banhammer_cron');
  variable_del('banhammer_custom');
  variable_del('banhammer_custom_message');
  variable_del('banhammer_geolocation_failure');
  variable_del('banhammer_geolocation_not_activated');
  variable_del('banhammer_geolocation_unknown');
  variable_del('banhammer_import');
  variable_del('banhammer_location');
  variable_del('banhammer_location_service');
  variable_del('banhammer_report_limit');
  variable_del('banhammer_spambot_blocking_enabled');
  variable_del('banhammer_threshold');
  variable_set('banhammer_timespan');
  drupal_uninstall_schema('banhammer');
}
