<?php

function banhammer_geolocation($form_state, $arg) {
  switch ($arg) {
    case "geoplugin":
      require_once(dirname(__FILE__) . "/banhammer.geolocation-mod.geoplugin.inc");
      $form = banhammer_geolocation_mod_geoplugin($form_state);
      break;
    default:
      return array(
        '#value' => 'Interesting... It seems that the geolocation service you\'re trying to use is not supported at this time!',
      );
  }
  $form['banhammer_geolocation_mod'] = array(
    '#type'  => 'value',
    '#value' => $arg,
  );
  return $form;
}

function banhammer_geolocation_validate($form, &$form_state) {
  switch ($form_state['values']['banhammer_geolocation_mod']) {
    case "geoplugin":
      require_once(dirname(__FILE__) . "/banhammer.geolocation-mod.geoplugin.inc");
      $form = banhammer_geolocation_mod_geoplugin_validate($form, &$form_state);
      break;
  }
}

function banhammer_geolocation_submit($form, &$form_state) {
  switch ($form_state['values']['banhammer_geolocation_mod']) {
    case "geoplugin":
      require_once(dirname(__FILE__) . "/banhammer.geolocation-mod.geoplugin.inc");
      $form = banhammer_geolocation_mod_geoplugin_submit($form, &$form_state);
      break;
  }
}

function banhammer_geolocation_validate_ip($ip) {
  if(preg_match("^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}^", $ip)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

?>
