<?php
// $Id$

/**
 * @file
 * Code to execute in hook_menu. Placed in a separate file to reduce overhead in banhammer.module.
 */

  switch ($path) {
    case 'admin/user/banhammer':
      // Blacklist Table
      return '<p>' . t('This page shows a table of IP addresses that have been banned from the site. You can remove any of these IP addresses or add them to the ' . l('Whitelist', 'admin/user/banhammer/whitelist') . '. If you select "Remove and ignore" BanHammer will remove the IP from the blacklist and will not show it again unless one or more spam filters blocks a submission from it.') . '</p>' . 
             '<p>' . t('If you have enabled spambot detection, you can blacklist IP addresses from the ' . l('Potential Spambots', 'admin/user/banhammer/spambots') . ' table. If you would like to manually blacklist IP addresses, you can do so from the ' . l('Access rules', 'admin/user/rules') . ' page (make sure you have ' . l('enabled', 'admin/user/banhammer/settings') . ' "Import Access Rules" first)') . '</p>';
      break;
    case 'admin/user/banhammer/spambots':
      // Potential Spambots Table
      return '<p>' . t('This page shows a table of IP addresses that meet the user-defined criteria for being a possible spambot. Also included in the table is the country of origin (if available), how many times the user has been stopped by a spam filter, and when was the last time a spam filter caught them.') . '</p>' .
             '<p>' . t('You can perform bulk actions by selecting some or all of the hosts in the table. If you choose "Ignore for now," then BanHammer will not show the IP address again unless one or more spam filters blocks a submission from it.') . '</p>';
      break;
    case 'admin/user/banhammer/whitelist':
      // Whitelist Table
      return '<p>' . t('This page shows a table of IP addresses that are exempt from being identified as a spammer. You can remove any of these IP addresses or add them to the blacklist. If you select "Remove and ignore" BanHammer will remove the IP from the whitelist and will not show it again unless one or more spam filters blocks a submission from it.') . '</p>' . 
             '<p>' . t('If you have enabled spambot detection, you can whitelist IP addresses from the ' . l('Potential Spambots', 'admin/user/banhammer/spambots') . ' table. If you would like to manually whitelist IP addresses, you can do so from the ' . l('Access rules', 'admin/user/rules') . ' page (make sure you have ' . l('enabled', 'admin/user/banhammer/settings') . ' "Import Access Rules" first)') . '</p>';
      break;
    default:
      return '';
      break;
  }
