CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Dependancies 
 * Installation
 * Usage
 * If you accidentally lock yourself out
 * TODO


INTRODUCTION
------------

Current Maintainer: Rob Sears <rob@robsears.com>

BanHammer analyzes the access log periodically to find bots that are failing spam filters. This can save a great deal of bandwidth if your site is being targeted by spambots, because once a host (ip address) is banned, Drupal will not serve any data to it. BanHammer works in tandem with Mollum and reCAPTCHA; when these modules identify spam/spammers, BanHammer considers whether to ban the ip address.


DEPENDANCIES
------------

BanHammer requires the following modules to be installed and enabled:

 * CAPTCHA (download: http://drupal.org/project/captcha)
 * dblog (included in core)

Users are recommended to add the reCAPTCHA and Mollum modules in order to better identify spam/spammers. Note that BanHammer does not identify spammers by the content they publish, but rather by their failure to pass spam filters.


INSTALLATION
------------

  1. Upload the banhammer folder to /sites/all/modules/ on your web server.

  2. From your Drupal website, navigate to /admin/build/modules/ and enable the BanHammer module. Ensure that the CAPTCHA and Database Logging modules are also enabled. If you have added the reCAPTCHA or Mollum modules, you should enable those too. Save the changes.

  3. After the modules have been activated, navigate to /admin/user/banhammer and tweak the settings to fit your need. You may also need to tweak any spam filter modules (CAPTCHA, reCAPTCHA, Mollum, etc) you have set up.


USAGE
-----

  1. Navigate to /admin/user/banhammer/hosts. This page shows a list of all hosts with failed CAPTCHA attemps that meet the criteria set up in the installation phase. You can add any of the listed hosts to the whitelist and they won't appear here again.

  2. You will see a link to automatically add the rest of the list to the blacklist. Clicking on this link will take you to a confirmation page. Confirming will add all of these hosts to Drupal's blacklist. Now when any of these hosts attempt to access any part of your site, they will be denied.

  3. You can edit the blacklist or whitelist at any time. Hosts can be removed from the blacklist, or moved from the blacklist to the whitelist. Conversely, hosts can be removed from the whitelist or moved from the whitelist to the blacklist.


IF YOU ACCIDENTALLY LOCK YOURSELF OUT
-------------------------------------

There are two ways to regain access to your Drupal site. You will either need to change your IP address or access your Drupal installation's MySQL table. The simplest way is the latter. Use a tool like PHPMyAdmin to open up the "access" table and remove your IP address from it.

If you can't do that, check out this tutorial: http://whatismyipaddress.com/change-ip

Another option would be to take a laptop to a place that offers free WiFi. Regardless, once you have obtained a different IP address, access the BanHammer blacklist, find your home IP address and move it to the whitelist.


TODO
----

 * Bug testing on different platforms/versions
 * Allow for the administrator to add a custom message about why a host was banned.
 * Modify bootstrap.inc to support the custom ban messages
 * Move to "User" Section
 * Doxygen commenting
 * Include hosts manually whitelisted or blacklisted to the Autoblocker lists
 * Link to manually add to the whitelist or blacklist
 * Check to make sure that CAPTCHA module is logging wrong responses
