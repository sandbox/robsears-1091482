<?php

function banhammer_geolocation_mod_geoplugin($form_state) {

  $ip     = variable_get('banhammer_geolocation_mod_geoplugin_ip', ((!empty($form_state['values']['banhammer_geolocation_mod_geoplugin_ip'])) ? $form_state['values']['banhammer_geolocation_mod_geoplugin_ip'] : $_SERVER['REMOTE_ADDR']));
  $ip_val = banhammer_geolocation_validate_ip($ip);

  $test   = 0;
  $data   = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
  foreach ($data as $key => $value) {
    if (!empty($value)) {
      $test++;
    }
  }
  $test   = ($ip_val) ? $test : 0;

  $form = array();
  $form['banhammer_geolocation_mod_geoplugin_intro'] = array(
    '#value' => '<strong>Note: </strong>the geoPlugin web analytics statistics package will <strong>not</strong> work until you tell geoPlugin to listen for your domain. And to do that, you need to ' . l('have an account', 'http://www.geoplugin.com/signup.php') . '. The registration process is simple and the service is free of charge. <strong>You must go through the registration process and add your site\'s domain for BanHammer to fetch geolocation for spambots</strong>. Read more at ' . l('http://www.geoplugin.com', 'http://www.geoplugin.com'),
  );

  $form['banhammer_geolocation_mod_geoplugin_test'] = array(
    '#title'       => t('GeoPlugin API Test'),
    '#type'        => 'fieldset',
    '#description' => t('Here you can verify your geolocation settings. If you have already set up an account with GeoPlugin, BanHammer will perform a quick check using your IP address to make sure everything is working.'),
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
  );

  $form['banhammer_geolocation_mod_geoplugin_test']['banhammer_geolocation_mod_geoplugin_results'] = array(
    '#prefix' => '<div class="messages ' . (($test === 0) ? 'error' : 'status') . '">',
    '#value'  => ($test === 0) ? 'Failed to fetch data for IP address ' . $ip : 'Success: IP address ' . $ip . ' is in ' . $data['geoplugin_city'] . ', ' . $data['geoplugin_region'] . ' (' . $data['geoplugin_countryName'] . ')',
    '#suffix' => '</div>',
  );

  $form['banhammer_geolocation_mod_geoplugin_test']['banhammer_geolocation_mod_geoplugin_ip'] = array(
    '#title'         => t('Alternate IP address to test'),
    '#type'          => 'textfield',
    '#description'   => t('If you would like BanHammer to verify your settings using a particular IP address other than your own, enter it here. Format: xx.xx.xx.xx'),
    '#default_value' => $ip,
  );

  $form['banhammer_geolocation_mod_geoplugin_test']['banhammer_geolocation_mod_geoplugin_retest'] = array(
    '#type'  => 'submit',
    '#value' => 'Retest',
  );

  return $form;
}

function banhammer_geolocation_mod_geoplugin_validate($form, $form_state) {
  $ip    = $form_state['values']['banhammer_geolocation_mod_geoplugin_ip'];
  if(!banhammer_geolocation_validate_ip($ip)) {
    form_set_error('banhammer_geolocation_mod_geoplugin_ip', 'The IP address you entered does not appear to be a valid IPv4 address. Please try another one, following the format xx.xx.xx.xx (note that geoPlugin does not yet support IPv6)');
  }
}

function banhammer_geolocation_mod_geoplugin_submit($form, $form_state) {
  variable_set('banhammer_geolocation_mod_geoplugin_ip', $form_state['values']['banhammer_geolocation_mod_geoplugin_ip']);
}

?>
