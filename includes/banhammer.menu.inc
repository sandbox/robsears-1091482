<?php
// $Id$

/**
 * @file
 * Code to execute in hook_menu. Placed in a separate file to reduce overhead in banhammer.module.
 */

  $items = array();

  $items['admin/user/banhammer'] = array(
    'title'            => 'BanHammer',
    'description'      => "Examines the log for spambots and adds their IP address to Drupal's 'Deny' list'",
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('banhammer_main', '0'),
    'type'             => MENU_NORMAL_ITEM,
    'file'             => 'includes/banhammer.admin.inc',
    'access arguments' => array(),
    'weight'           => 0,
  );

  if (variable_get('banhammer_spambot_blocking_enabled', 1) == 1) {
    $items['admin/user/banhammer/spambots'] = array(
      'title'            => 'Potential Spambots',
      'description'      => 'This is a list of IP addresses that are suspected of being spambots.',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('banhammer_main', '2'),
      'type'             => MENU_LOCAL_TASK,
      'file'             => 'includes/banhammer.admin.inc',
      'access arguments' => array(),
      'weight'           => 0,
    );
  }

  $items['admin/user/banhammer/blacklist'] = array(
    'title'            => 'Blacklist',
    'description'      => 'This is a list of IP addresses that have been marked as spambots.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('banhammer_main', '0'),
    'type'             => MENU_DEFAULT_LOCAL_TASK,
    'file'             => 'includes/banhammer.admin.inc',
    'access arguments' => array(),
    'weight'           => -7,
  );

  $items['admin/user/banhammer/whitelist'] = array(
    'title'            => 'Whitelist',
    'description'      => 'This is a list of IP addresses that are exempt from being monitored.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('banhammer_main', '1'),
    'type'             => MENU_LOCAL_TASK,
    'file'             => 'includes/banhammer.admin.inc',
    'access arguments' => array(),
    'weight'           => -5,
  );

  if (variable_get('banhammer_location', 0) == 1) {
    $mods = array_keys(banhammer_get_supported_geolocation());
    $arg  = variable_get('banhammer_location_service', $mods[0]);
    $items['admin/user/banhammer/geolocation'] = array(
      'title'            => 'Geolocation',
      'description'      => 'Manage the geolocation settings',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('banhammer_geolocation', $arg),
      'type'             => MENU_LOCAL_TASK,
      'file'             => 'includes/banhammer.geolocation.inc',
      'access arguments' => array(),
      'weight'           => 1,
    );
  }

  $items['admin/user/banhammer/settings'] = array(
    'title'            => 'Settings',
    'description'      => 'Manage the behavior of BanHammer',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('banhammer_settings'),
    'type'             => MENU_LOCAL_TASK,
    'file'             => 'includes/banhammer.settings.inc',
    'access arguments' => array(),
    'weight'           => 3,
  );

  // The function 'banhammer_custom_messaging_title' is located in
  // banhammer.module. Break in convention was necessary to
  // function properly.
  $items['admin/user/banhammer/edit-ban-message/%'] = array(
    'title callback'   => 'banhammer_custom_messaging_title',
    'title arguments'  => array(4), 
    'description'      => 'Manage the behavior of BanHammer',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('banhammer_custom_messaging', 4),
    'type'             => MENU_CALLBACK,
    'file'             => 'includes/banhammer.admin.inc',
    'access arguments' => array(),
    'weight'           => 0,
  );

  $items['admin/user/banhammer/update'] = array(
    'page callback'    => 'banhammer_main_submit',
    'type'             => MENU_CALLBACK,
    'file'             => 'includes/banhammer.admin.inc',
    'access arguments' => array(),
  );

  return $items;
