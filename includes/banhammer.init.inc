<?php
// $Id$

/**
 * @file
 * Code to execute on URI request. Placed in separate file to reduce overhead in banhammer.module.
 */

  // If administrator has enabled custom ban messages, interrupt Drupal's
  // default load process:
  if (variable_get('banhammer_custom', 0) == 1) {
    if (db_result(db_query("SELECT COUNT(*) FROM {banhammer} WHERE status='0' AND host_ip='%s'", $_SERVER['REMOTE_ADDR'])) > 0) {
      $message = db_fetch_array(db_query("SELECT * FROM {banhammer} WHERE status='0' AND host_ip='%s'", $_SERVER['REMOTE_ADDR']));
      print (!empty($message['message']))
        ? $message['message']
        : variable_get('banhammer_custom_message', '');
      exit();
    }
  }

