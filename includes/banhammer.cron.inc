<?php
// $Id$

/**
 * @file
 * Code to execute on CRON run. Placed in separate file to reduce overhead in banhammer.module.
 */


  if (variable_get('banhammer_cron', 0) == 1) {

    // Initialize variables:
    $ips           = array();
    $timespan      = (variable_get('banhammer_timespan',  '7')) * 24 * 60 * 60;
    $start         = time() - $timespan;
    $threshold     = variable_get('banhammer_threshold', '3');
    $fetch_country = variable_get('banhammer_location', 0);

    // Build an array of IPs that have failed CAPTCHAs over the given time period
    $sql        = banhammer_build_sql();
    $result     = db_query($sql, $start);

    while($row = db_fetch_array($result)) {
      $failures = (isset($ips[$row['hostname']]['count']))
        ? $ips[$row['hostname']]['count'] + 1
        : 1;
      $ips[$row['hostname']] = array(
        'count'   => $failures,
        'last'    => $row['timestamp'],
        'country' => 'Not Installed',
      );
    }

    $i = 0;
    foreach (array_keys($ips) as $key) {
      $host = db_fetch_array(db_query("SELECT * FROM {banhammer} WHERE host_ip='%s'", $key));
      if (empty($host) && $key != $_SERVER['REMOTE_ADDR']) {
        $object = array(
          'host_ip'         => $key,
          'host_country'    => '',
          'failed_attempts' => $ips[$key]['count'],
          'last_attempt'    => $ips[$key]['last'],
          'status'          => '0',
          'message'         => variable_get('banhammer_custom_message', ''),
        );
        drupal_write_record('banhammer', $object);
        $i++;
      }
    }
    drupal_set_message('Added ' . $i . ' hosts to the BanHammer blacklist.', 'status', FALSE);
  }


